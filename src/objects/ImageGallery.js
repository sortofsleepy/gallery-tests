import Mesh from '../jirachi/framework/Mesh'
import createPlane from './createPlane'
import {createTexture2d} from '../jirachi/core/texture'
import {createFBO,createFBOAttachmentFormat} from '../jirachi/core/fbo'
import DrawQuad from '../jirachi/drawquad'
import {flattenArray} from '../jirachi/math/core'

import imagev from '../shaders/image.vert'
import imagef from '../shaders/image.glsl'
import sim from '../shaders/sim.glsl'

class ImageGallery {
    constructor(gl,{
        images=[],
        width=640,
        height=480,
        threshold=0.3
    }={}){
        this.gl = gl;

        // set dimensions - half values so we can
        // minimize the number of vertices that need to be drawn.
        this.width = width * 0.5;
        this.height = height * 0.5;

        // radius of particles when not in display mode.
        this.radius = 100.0;

        this.threshold = threshold;

        this.shouldDisplayImage = false;

        this.props = {
            mix:1
        }


        // images
        this.images = images;

        // reference to context
        this.gl = gl;

        this.numImages = images.length - 1;

        // index to keep track of so we know which images to queue up
        this.index = 0;

        // current texture to show
        this.currentTexture = this.images[this.index].texture;

        // reference to the next
        this.nextTexture = this.images[this.index].texture;

        // flag for whether or not we need to animate the transition
        this.animateTransition = false;

        // flag to indicate if we're animating or not.
        this.isAnimating = false;

        // load mix texture
        let img = new Image();
        img.src = "/img/transition.png";
        img.onload = () => {
            this.mixTexture = createTexture2d(gl,{
                data:img
            });
            this.mixTextureLoaded = true;
        };


        this.isAnimating = false;

        window.addEventListener('keydown',() => {
            this.shouldDisplayImage = !this.shouldDisplayImage;
        })


        this._buildMesh();
    }

    /**
     * Update FBO ping-ponging
     * @private
     */
    _updateFBO(){

        let gl = this.gl;

        this.target.bind();
        gl.clearScreen();

        this.current.getTexture().bind(0);
        this.current.getTexture(1).bind(1)
        this.initialPositions.bind(2);
        this.displayPositions.bind(3);
        gl.setViewport(0,0,this.width + 1, this.width + 1);

        this.drawQuad.draw(shader => {
            shader.setTextureUniform('positionTex',0);
            shader.setTextureUniform('velocityTex',1);
            shader.setTextureUniform('initialPositions',2);
            shader.setTextureUniform('displayPositions',3);

            shader.setBooleanUniform('shouldShowImage',this.shouldDisplayImage)
        })

        gl.clearTextures();
        this.target.unbind();
        let tmp = this.current;
        this.current = this.target;
        this.target = tmp;
    }

    draw(camera){
        let gl = this.gl;
        this._updateFBO();

        gl.setViewport()


        if(this.mixTextureLoaded){
            this.currentTexture.bind();
            this.nextTexture.bind(1);
            this.mixTexture.bind(2);


            this.target.getTexture().bind(3);
            this.mesh.draw(camera,(shader)=>{

                shader.setBooleanUniform('animateTransition',this.animateTransition);
                shader.uniform("threshold",this.threshold);

                shader.setTextureUniform('currentImage',0);
                shader.setTextureUniform('nextImage',1);
                shader.setTextureUniform("mixTexture",2);
                shader.setTextureUniform('positionTexture',3);

                shader.setBooleanUniform('shouldDisplayImage',this.shouldDisplayImage);

                shader.uniform("time",performance.now() * 0.0001);
                shader.uniform('mixRatio',this.props.mix);

            });

            gl.clearTextures();
        }
    }

    // ================= GALLERY HANDLING ===================== //
    /**
     * Controls tweening of one image to another.
     */
    tweenProps(){
        let self = this;
        TweenMax.to(this.props,1.0,{
            mix:0,
            onComplete(){

                self.animateTransition = false;
                self.currentTexture = self.nextTexture;
                self.props.mix = 1;
                self.isAnimating = false;
            }
        })
    }

    /**
     * Navigates to the next image
     */
    next(){
        if(!this.isAnimating){
            this.nextTexture = this.images[this.getNextImage()].texture;
            this._animateNext()
            this.isAnimating = true;
        }

        this.tweenProps();
    }

    /**
     * Navigates to the previous image
     */
    previous(){
        if(!this.isAnimating){
            this.nextTexture = this.images[this.getPreviousImage()].texture;
            this._animatePrevious()
            this.isAnimating = true;
        }
        this.tweenProps();
    }

    /**
     * Increments index counter.
     */
    _animateNext(){
        this.animateTransition = true;
        if(this.index + 1 < this.numImages){
            this.index++;
        }
    }

    /**
     * Decrements index counter.
     * @private
     */
    _animatePrevious(){
        this.animateTransition = true;

        if(this.index > 0){
            this.index -= 1;
        }
    }


    /**
     * Return the next image index based on the current index.
     * If we're at the end - return 0 so binding code still works
     * @returns {number}
     */
    getNextImage(){
        if(this.index < this.numImages){
            return this.index + 1;
        }else{
            return 0
        }
    }

    /**
     * Return the previous image index based on the current index.
     * If we're at the start - return 0 so binding code still works
     * @returns {number}
     */
    getPreviousImage(){
        if(this.index !== 0){
            return this.index;
        }else{
            return 0;
        }
    }



    // =================== MESH CONSTRUCTION ================== //

    _buildMesh(){
        let gl = this.gl;

        // gonna use scaled width/height to help reduce vertex count.
        let width = this.width;
        let height = this.height;

        // ====== BUILD REVEAL POSITION / UVS etc ======== //
        let positions = [];
        let texPositions = [];
        let uvs = []
        let normals = []
        let sx = width;
        let sy = height;
        let resolution = 1;
        let nx = width / resolution;
        let ny = height / resolution;
        let numCount = 0;
        for (var iy = 0; iy <= ny; iy++) {
            for (var ix = 0; ix <= nx; ix++) {
                var u = ix / nx
                var v = iy / ny


                // flip uvs so texture looks right when displaying
                u = 1.0 - u;
                v = 1.0 - v;

                var x = (ix * resolution) - (sx / 2);
                var y = (iy * resolution * -1) + (sy / 2);

                positions.push(x, y, 0)
                texPositions.push(x,y,0,1);
                uvs.push([u, v])
                normals.push([0, 0, 1])
                numCount += 1;
            }
        }

        // build UV coordinates so we can properly query textures.
        let aUV = [];
        let nump = this.width + 1;
        for(let j = 0; j < nump; j += 1) {
            for (let i = 0; i < nump; i += 1) {
                let u = i / nump;
                let v = j / nump;

                aUV.push(u, v);
            }
        }


        // figure out possible positions along a circle.
        let deg2Rad = Math.PI / 180;
        let circlePositions = [];
        for(let i = 0; i < 360; ++i){
            let degInRad = i * deg2Rad;
            let x = Math.cos(degInRad) * this.radius;
            let y = Math.sin(degInRad) * this.radius;
            circlePositions.push([x,y,0,0]);
        }

        // assign a random position on the circle. This serves as the starting position.
        let initialPositions = [];

        // random sizes for each particle when not in display mode.
        let psizes = [];

        // positions for filler mesh
        let fillerPositions = [];

        let numParticles = positions.length;
        for(let i = 0; i < numParticles;++i){

            let p = circlePositions[Math.floor(Math.random() * 360)];

            // offset things slightly.
            p[0] += Math.random() - 0.5;
            p[1] += Math.random() - 0.5;

            // push initial particle positions in standby mode.
            initialPositions.push(p);

            // push filler positions
            fillerPositions.push(p);

            // push random sizes for each particle in standby mode.
            psizes.push(range(-2,2));
        }



        // ============= BUILD MESH =================== //
        // build renderable mesh
        let plane = createPlane(1,1);

        let mesh = new Mesh(gl,{
            vertex:imagev,
            fragment:imagef,
            uniforms:[
                'shouldDisplayImage',
                'currentImage',
                'nextImage',
                'mixRatio',
                'threshold',
                'mixTexture',
                'animateTransition',
                'positionTexture',
                'uScreen'
            ]
        });


        // add attributes for forming main mesh
        mesh.addAttribute('position',plane.positions);

        // the current position for all instances when not in the display position.
        mesh.addInstancedAttribute('ppos',initialPositions);
//        mesh.addInstancedAttribute('ppos',positions);

        mesh.addInstancedAttribute('scaleSize',psizes,1);

        mesh.addInstancedAttribute('aUV',aUV,2);
        mesh.addInstancedAttribute('uv',uvs,2);

        // add indices
        mesh.addIndices(plane.cells);

        // set number of instances to draw
        // should match the number of instance positions.
        mesh.numInstances = positions.length / 3;


        this.mesh = mesh;

        this._buildBuffers(texPositions,flattenArray(initialPositions,4));

    }

    _buildBuffers(texPositions,initialPositions){

        // base dimension for all textures
        let nump= this.width + 1;

        // first set of textures
        let textures1 = [];
        let textures2 = [];

        // ============== BUILD DISPLAY POSITIONS ===================== //
        let displayData = new Float32Array(nump * nump * 4);
        texPositions.forEach((itm,idx)=>{
            displayData[idx] = itm;
        });


        // constant texture holding the necessary positions return paritcles
        // to display position.
        this.displayPositions = createTexture2d(this.gl,{
            width:nump,
            height:nump,
            floatingPoint:true,
            data:displayData
        });


        // ============= BUILD STARTING POSITIONS ================== //
        let startData = new Float32Array(nump * nump * 4);
        initialPositions.forEach((itm,idx) => {
            startData[idx] = itm;
        })

        this.initialPositions = createTexture2d(this.gl,{
            width:nump,
            height:nump,
            floatingPoint:true,
            data:startData
        });

        let startPos = new Float32Array(nump * nump * 4);
        startPos.forEach(itm => {
            itm = Math.random()
        })
        textures1.push(
            createTexture2d(this.gl,{
                width:nump,
                height:nump,
                floatingPoint:true,
                data:startData
            }),
            createTexture2d(this.gl,{
                width:nump,
                height:nump,
                floatingPoint:true,
                randomInit:true,
            })
        );

        textures2.push(
            createTexture2d(this.gl,{
                width:nump,
                height:nump,
                floatingPoint:true,
                data:startData
            }),
            createTexture2d(this.gl,{
                width:nump,
                height:nump,
                floatingPoint:true,
                randomInit:true,
            })
        );


        this.target = createFBO(this.gl,{
            width:nump,
            height:nump,
            format:createFBOAttachmentFormat(this.gl,{
                textures:textures1
            })
        })


        this.current = createFBO(this.gl,{
            width:nump,
            height:nump,
            format:createFBOAttachmentFormat(this.gl,{
                textures:textures2
            })
        })

        this.drawQuad = new DrawQuad(this.gl,{
            fragment:sim,
            uniforms:[
                'positionTex',
                'velocityTex',
                'initialPositions',
                'displayPositions',
                'shouldShowImage'
            ]
        });
    }
}

export default ImageGallery;