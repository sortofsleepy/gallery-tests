

in vec2 vUv;
uniform sampler2D positionTex;
uniform sampler2D velocityTex;

uniform sampler2D initialPositions;
uniform sampler2D displayPositions;

uniform bool shouldShowImage;

layout (location = 0) out vec4 oPos;
layout (location = 1) out vec4 oVel;

#define HALF_PI 1.5707963267948966
float elasticInOut(float t) {
  return t < 0.5
    ? 0.5 * sin(+13.0 * HALF_PI * 2.0 * t) * pow(2.0, 10.0 * (2.0 * t - 1.0))
    : 0.5 * sin(-13.0 * HALF_PI * ((2.0 * t - 1.0) + 1.0)) * pow(2.0, -10.0 * (2.0 * t - 1.0)) + 1.0;
}


void main(){
    vec4 pos = texture(positionTex,vUv);
    vec4 iVel = texture(velocityTex,vUv);

    vec4 initialPos = texture(initialPositions,vUv);
    vec4 displayPos = texture(displayPositions,vUv);

    float dx = 0.0;
    float dy = 0.0;


    if(shouldShowImage){

     dx = (displayPos.x) - pos.x;
     dy = (displayPos.y) - pos.y;
     pos.x += (dx * 0.05);
     pos.y += (dy * 0.05);

    }else{
        dx = (initialPos.x) - pos.x;
        dy = (initialPos.y) - pos.y;

        pos.x += (dx * 0.05);
        pos.y += (dy * 0.05);
    }


    oPos = pos;
    oVel = iVel;
}