uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform sampler2D positionTexture;
uniform float time;

in vec3 position;
in float scaleSize;
in vec2 uv;

// instanced variables
in vec3 ppos;
in vec2 aUV;


out vec2 vUv;


void main(){
    vec3 startPos = position;
    vec3 iPos = ppos;

    float scale = scaleSize;

    startPos.x *= scale;
    startPos.y *= scale;


    vec4 posData = texture(positionTexture,aUV);

    // calculate final positions;
    vec3 pos = startPos + posData.rgb;
    //vec3 pos = startPos + iPos;

    vUv = uv;

    gl_Position = projectionMatrix * viewMatrix * vec4(pos,1.);
    //gl_Position = projectionMatrix * modelMatrix * viewMatrix * vec4(pos,1.);
}
